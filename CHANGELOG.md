# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0] - 2021-09-04
### Added
- Color Correction.
- Missing icon.

## [1.4.1] - 2021-09-04
### Added
- Licence.

## [1.4.2] - 2021-09-04
### Fixed
- Bug fix.

## [1.4.6] - 2021-09-04
### Fixed
- Balanced some minor problems.

## [1.4.8] - 2021-09-06
### Updated
- Reviewed the color palettes.

## [2.0.0] - 2021-09-11
### Updated
- Major Update.
- Made the theme more seemless black.

## [2.0.1] - 2021-09-11
### Updated
- Minor changes.

## [2.0.2] - 2021-09-11
### Updated
- Color tweaks.

## [2.0.3] - 2021-09-11
### Updated
- More color tweaks.

## [2.0.4] - 2021-09-11
### Updated
- Updated readme file.
- Made the overview border black.

## [2.0.5] - 2021-09-12
### Updated
- Updated readme file.
- Updated color of matching brackets boxes.
- Updated background color for the highlight of line at the cursor position.
- Updated color palettess.

## [2.0.6] - 2021-09-13
### Updated
- Updated terminal text color.
- Updated text highlight color.

## [3.0.0] - 2021-09-14
### Updated
- Major Overhaul of usability.

## [3.1.0] - 2021-09-23
### Updated
- Updated editor colors.
- Updated color palette.

## [3.2.0] - 2021-09-30
### Updated
- Updated highlight color.
- Updated tab hover background color.
- Updated Indent guide color.
- Rearranged color palette for better look in HTML and CSS .

## [3.2.1] - 2021-09-30
### Updated
- Updated readme file.

## [4.0.0] - 2021-10-01
### Added
- Added italic style font version along with the normal version.
- Updated Indent active guide color.

## [4.0.1] - 2021-10-01
### Updated
- Updated readme file.

## [4.3.0] - 2021-10-02
### Updated
- Updated italic text configurations.
- The non-italic version is now default, Italic version is 2nd choice.
- Updated readme file.

## [5.0.0] - 2021-10-08
### Added
- Another major update.
- Added Midnight Black color theme with italic support.
